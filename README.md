## Location APi

***An API that aggregates Locations,images and metadata about those images from Google Places API***

## **How to set the project up**

1. Make sure you have Microsoft SQL Server Installed
2. Go to **appsettings.Development**
3. Under **ConnectionStrings** replace **PlacesDb** Value Which is **Data Source=.;Initial Catalog=PlacesDb;Integrated Security=True;TrustServerCertificate=True** with own your connection string. (**NB Replace the value not the key [PlacesDb]**)
4. Build and run the App

**NB //The WebApi will automatically create all the Database tables and stored procs for you**
 
**You are good to go!**
