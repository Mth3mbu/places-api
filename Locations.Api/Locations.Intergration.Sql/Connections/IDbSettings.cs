﻿namespace Locations.Intergration.Sql.Connections
{
    public interface IDbSettings
    {
        public string ConnectionString { get; }
    }
}
