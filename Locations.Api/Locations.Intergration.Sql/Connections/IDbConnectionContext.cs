﻿using System.Data;

namespace Locations.Intergration.Sql.Connections
{
    public interface IDbConnectionContext
    {
        IDbConnection GetConnection();
        IDbTransaction GetTransaction();
        void Commit();
        void Rollback();
    }
}
