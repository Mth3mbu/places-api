﻿using Dapper;
using Locations.Domain.Auth;
using Locations.Domain.Users;
using Locations.Intergration.Sql.Connections;
using System.Data;

namespace Locations.Intergration.Sql.Users
{
    public class UserGateway : IUserGateway
    {
        private readonly ILocationsDbConnectionContext _context;
        private IDbTransaction DbTransaction => _context.GetTransaction();
        private IDbConnection DbConnection => _context.GetConnection();

        public UserGateway(ILocationsDbConnectionContext salesDbConnectionContext)
        {
            _context = salesDbConnectionContext;
        }

        public async Task<User> FindByEmail(string email)
        {
            var user = await DbConnection.QuerySingleOrDefaultAsync<User>("SELECT TOP (1) [Id],[Name],[LastName],[Email] From [User] WHERE [Email]=@email",
                  new { email },
                  transaction: DbTransaction);

            return user;
        }

        public async Task<User> FindByUserId(Guid userId)
        {
            var user = await DbConnection.QuerySingleOrDefaultAsync<User>("SELECT TOP (1) [Id],[Name],[LastName],[Email] From [User] WHERE [Id]=@userId",
                   new {userId},
                   transaction: DbTransaction);

            return user;
        }
    }
}
