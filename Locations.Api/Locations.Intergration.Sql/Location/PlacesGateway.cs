﻿using Dapper;
using Locations.Domain.Location;
using Locations.Intergration.Sql.Connections;
using System.Data;

namespace Locations.Intergration.Sql.Location
{
    public class PlacesGateway : IPlacesGateway
    {
        private readonly ILocationsDbConnectionContext _dbConnectionContext;
        private IDbTransaction DbTransaction => _dbConnectionContext.GetTransaction();
        private IDbConnection DbConnection => _dbConnectionContext.GetConnection();

        public PlacesGateway(ILocationsDbConnectionContext dbConnectionContext)
        {
            _dbConnectionContext = dbConnectionContext;
        }

        public async Task CreatePlaces(IEnumerable<Place> places)
        {
            await DbConnection.ExecuteAsync("spInsertPlace", places.ToArray(), commandType: CommandType.StoredProcedure, transaction: DbTransaction);
        }

        public async Task<List<Place>> GetPlacesByName(string locationName)
        {
            var places = await DbConnection.QueryAsync<Place>("spGetPlacesByName", new { SearchTerm = locationName }, commandType: CommandType.StoredProcedure, transaction: DbTransaction);

            return places.ToList();
        }

        public async Task<Place> GetPlacesById(string placeId)
        {
            return await DbConnection.QueryFirstOrDefaultAsync<Place>("spGetPlaceById", new { PlaceId = placeId }, commandType: CommandType.StoredProcedure, transaction: DbTransaction);
        }
    }
}
