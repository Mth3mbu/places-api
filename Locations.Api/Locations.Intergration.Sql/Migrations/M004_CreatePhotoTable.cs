﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201220930)]
    public class M004_CreatePhotoTable : Migration
    {
        public override void Down()
        {

        }

        public override void Up()
        {
            Create.Table("Photo")
                  .WithColumn("Id").AsGuid().PrimaryKey()
                  .WithColumn("PlaceId").AsString(100)
                  .ForeignKey("fk_place_id", "Place", "Id")
                  .WithColumn("Photo").AsString(int.MaxValue);
        }
    }
}
