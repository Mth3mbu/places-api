﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201220932)]
    public class M006_CreateSpGetPhotosByPlaceIdProc : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spGetPhotosByPlaceId 
                                                    @PlaceId nvarchar(1000)
                                                    AS
                                                    BEGIN
                                                    SELECT [Id]
                                                          ,[PlaceId]
                                                          ,[Photo]
                                                      FROM [Photo] WHERE [PlaceId]=@PlaceId
                                                    END"); 
        }
    }
}
