﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201221214)]
    public class M007_CreateSpGetPlaceById : Migration
    {
        public override void Down()
        {
           
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spGetPlaceById
                                            @PlaceId nvarchar(100)
                                            AS
                                            BEGIN
                                            SELECT TOP (1) [Id]
                                                  ,[Name]
                                                  ,[Longitude]
                                                  ,[Latitude]
                                                  ,[Address]
                                              FROM [Place] WHERE [Id]=@PlaceId
                                            END");
        }
    }
}
