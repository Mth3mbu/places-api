﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201211646)]
    public class M003_CreateProcGetPlacesByName : Migration
    {
        public override void Down()
        {

        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spGetPlacesByName
														@SearchTerm nvarchar(200)
                                                        AS
                                                        BEGIN
                                                        SELECT [Id]
                                                              ,[Name]
                                                              ,[Longitude]
                                                              ,[Latitude]
                                                              ,[Address]
                                                          FROM [Place] WHERE [Name] Like '%'+@SearchTerm +'%' OR [SearchTerm] LIKE '%'+@SearchTerm+'%'
                                                        END");
        }
    }
}
