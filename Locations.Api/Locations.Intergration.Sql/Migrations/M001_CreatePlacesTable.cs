﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201211624)]
    public class M001_CreatePlacesTable : Migration
    {
        public override void Down()
        {
          
        }

        public override void Up()
        {
            Create.Table("Place")
                .WithColumn("Id").AsString(100).NotNullable().PrimaryKey()
                .WithColumn("Name").AsString(200)
                 .WithColumn("SearchTerm").AsString(200)
                 .WithColumn("Longitude").AsDouble()
                 .WithColumn("Latitude").AsDouble()
                .WithColumn("Address").AsString(500);
        }
    }
}
