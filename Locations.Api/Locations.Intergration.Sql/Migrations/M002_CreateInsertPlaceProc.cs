﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201211645)]
    public class M002_CreateInsertPlaceProc : Migration
    {
        public override void Down()
        {

        }

        public override void Up()
        {

            Execute.Sql(@"CREATE PROC spInsertPlace
                                            @Id varchar(100),
                                            @Name varchar(200),
                                            @Longitude float,
                                            @Latitude float,
                                            @Address varchar(500),
											@SearchTerm varchar(200)
                                        AS
                                        BEGIN
                                        INSERT INTO[Place]
                                                   ([Id]
                                                   ,[Name]
                                                   ,[Longitude]
                                                   ,[Latitude]
                                                   ,[Address]
												   ,[SearchTerm]
                                                   )
                                             VALUES
                                                  (@Id
                                                  ,@Name 
                                                  ,@Longitude
                                                  ,@Latitude
                                                  ,@Address
												  ,@SearchTerm
                                                  )
                                        END");
        }
    }
}
