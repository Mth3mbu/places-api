﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201221746)]
    public class M10_CreateSpLoginProc : Migration
    {
        public override void Down()
        {
        
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spLogin
                                            @Email nvarchar(255)
                                            AS
                                            BEGIN
                                            SELECT[Id]
                                                  ,[Name]
                                                  ,[LastName]
                                                  ,[Email]
                                                  ,[Password]
                                              FROM[User] WHERE[Email] = @Email
                                            END");
        }
    }
}
