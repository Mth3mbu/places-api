﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201232046)]
    public class M13_CreateSpAlterProcInsertPlaces : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.Sql(@"ALTER PROC spInsertPlace
                                            @Id varchar(100),
                                            @Name varchar(200),
                                            @Longitude float,
                                            @Latitude float,
                                            @Address varchar(500),
											@SearchTerm varchar(200),
                                            @UserId uniqueidentifier
                                        AS
                                        BEGIN
                                        INSERT INTO[Place]
                                                   ([Id]
                                                   ,[Name]
                                                   ,[Longitude]
                                                   ,[Latitude]
                                                   ,[Address]
												   ,[SearchTerm]
                                                   ,[UserId]
                                                   )
                                             VALUES
                                                  (@Id
                                                  ,@Name 
                                                  ,@Longitude
                                                  ,@Latitude
                                                  ,@Address
												  ,@SearchTerm
                                                  ,@UserId)
                                        END");
        }
    }
}
