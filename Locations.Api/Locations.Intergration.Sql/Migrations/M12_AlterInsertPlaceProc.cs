﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201222022)]
    public class M12_AlterInsertPlaceProc : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.Sql(@"ALTER PROC spInsertPlace
                                            @Id varchar(100),
                                            @Name varchar(200),
                                            @Longitude float,
                                            @Latitude float,
                                            @Address varchar(500),
                                            @UserId uniqueidentifier
                                        AS
                                        BEGIN
                                        INSERT INTO[Place]
                                                   ([Id]
                                                   ,[Name]
                                                   ,[Longitude]
                                                   ,[Latitude]
                                                   ,[Address]
                                                   ,[UserId]
                                                   )
                                             VALUES
                                                  (@Id
                                                  ,@Name 
                                                  ,@Longitude
                                                  ,@Latitude
                                                  ,@Address
                                                  ,@UserId)
                                        END");
        }
    }
}
