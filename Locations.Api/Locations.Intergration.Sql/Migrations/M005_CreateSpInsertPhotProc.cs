﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201220931)]
    public class M005_CreateSpInsertPhotProc : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spInsertPhoto
                                            @Id uniqueidentifier
                                          , @PlaceId nvarchar(100)
                                          , @Photo nvarchar(max)
                                        As
                                        BEGIN
                                        INSERT INTO[Photo]
                                                   ([Id]
                                                   ,[PlaceId]
                                                   ,[Photo])
                                             VALUES
                                                   (@Id
                                                   , @PlaceId
                                                   , @Photo)
                                        END");
        }
    }
}
