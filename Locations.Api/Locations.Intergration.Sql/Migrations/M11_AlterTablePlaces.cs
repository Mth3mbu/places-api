﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201221949)]
    public class M11_AlterTablePlaces : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.Sql(@"ALTER TABLE [Place]
                            ADD UserId uniqueidentifier FOREIGN KEY(UserId) REFERENCES [User](Id);");
        }
    }
}
