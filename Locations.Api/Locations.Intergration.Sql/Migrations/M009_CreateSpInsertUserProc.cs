﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201221736)]
    public class M009_CreateSpInsertUserProc : Migration
    {
        public override void Down()
        {

        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spInsertUser
                                            @Id uniqueidentifier,
                                            @Name nvarchar(255),
                                            @LastName nvarchar(255),
                                            @Email nvarchar(255),
                                            @Password nvarchar(255)
                                            AS
                                            BEGIN
                                            INSERT INTO [User]
                                                       ([Id]
                                                       ,[Name]
                                                       ,[LastName]
                                                       ,[Email]
                                                       ,[Password])
                                                 VALUES
                                                       (@Id 
                                                       ,@Name
                                                       ,@LastName
                                                       ,@Email
                                                       ,@Password)
                                            END
                                            ");
        }
    }
}
