﻿using FluentMigrator;

namespace Locations.Intergration.Sql.Migrations
{
    [Migration(202201221215)]
    public class M008_CreateUserTable : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Create.Table("User").WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("LastName").AsString().NotNullable()
                .WithColumn("Email").AsString().NotNullable().Unique()
                .WithColumn("Password").AsString().NotNullable();
        }
    }
}
