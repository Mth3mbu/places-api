﻿using Dapper;
using Locations.Domain.Auth;
using Locations.Intergration.Sql.Connections;
using System.Data;

namespace Locations.Intergration.Sql.Auth
{
    public class AuthGateway : IAuthGateway
    {
        private readonly ILocationsDbConnectionContext _context;
        private IDbTransaction DbTransaction => _context.GetTransaction();
        private IDbConnection DbConnection => _context.GetConnection();

        public AuthGateway(ILocationsDbConnectionContext salesDbConnectionContext)
        {
            _context = salesDbConnectionContext;
        }
        public async Task<User> Login(LoginRequest request)
        {
            return await DbConnection.QuerySingleOrDefaultAsync<User>("spLogin", new { request.Email},
                commandType: CommandType.StoredProcedure,
                transaction: DbTransaction);
        }

        public async Task<User> Register(User user)
        {
            await DbConnection.ExecuteAsync("spInsertUser", user, commandType: CommandType.StoredProcedure, transaction: DbTransaction);

            return user;
        }
    }
}
