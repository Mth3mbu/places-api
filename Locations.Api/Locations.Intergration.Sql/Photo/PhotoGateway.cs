﻿using Dapper;
using Locations.Domain.Photos;
using Locations.Intergration.Sql.Connections;
using System.Data;

namespace Locations.Intergration.Sql.Photo
{
    public class PhotoGateway : IPhotoGateway
    {
        private readonly ILocationsDbConnectionContext _dbConnectionContext;
        private IDbTransaction DbTransaction => _dbConnectionContext.GetTransaction();
        private IDbConnection DbConnection => _dbConnectionContext.GetConnection();

        public PhotoGateway(ILocationsDbConnectionContext dbConnectionContext)
        {
            _dbConnectionContext = dbConnectionContext;
        }

        public async Task CreatePhoto(List<Image> photos)
        {
            await DbConnection.ExecuteAsync("spInsertPhoto", photos, commandType: CommandType.StoredProcedure, transaction: DbTransaction);
        }

        public async Task<List<Image>> GetPhotosByPlaceId(string placeId)
        {
            var results = await DbConnection.QueryAsync<Image>("spGetPhotosByPlaceId", new { PlaceId = placeId }, commandType: CommandType.StoredProcedure, transaction: DbTransaction);

            return results.ToList();
        }
    }
}
