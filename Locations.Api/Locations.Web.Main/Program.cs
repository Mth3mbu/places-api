using FluentMigrator.Runner;
using FluentValidation.AspNetCore;
using Locations.Domain.Auth;
using Locations.Web.Main.Infrastructure;
using Locations.Web.Main.IoCConfig;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = Microsoft.OpenApi.Models.SecuritySchemeType.Http,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."
    });
    options.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement {
        {
            new Microsoft.OpenApi.Models.OpenApiSecurityScheme {
                    Reference = new Microsoft.OpenApi.Models.OpenApiReference {
                        Type = Microsoft.OpenApi.Models.ReferenceType.SecurityScheme,
                            Id = "Bearer"
                    }
                },
                new string[] {}
        }
    });

    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Locations API",
        Description = "An API that aggregates Locations,images and metadata about those images from Google Places API",
        TermsOfService = new Uri("https://example.com/terms"),
    });

    // using System.Reflection;
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});


builder.Services
    .AddGooglePlaces()
    .AddPlaces()
    .AddAuth()
    .AddAuthentication(builder.Configuration)
    .AddUser()
    .AddPhotos()
    .AddPresenters()
    .AddMigrations()
    .AddSqlIntegration().AddFluentValidation(config =>
    {
        config.DisableDataAnnotationsValidation = true;
        config.RegisterValidatorsFromAssemblyContaining<UserValidator>();
        config.RegisterValidatorsFromAssemblyContaining<LoginRequestValidator>();
    }); ;

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

SetCors(app, app.Configuration);

RunDbMigrator(app);

app.Run();

void SetCors(IApplicationBuilder app, IConfiguration configuration)
{
    var corsSettings = configuration.Read<CorsSettings>("Cors");
    app.UseCors(builder => builder
        .WithOrigins(corsSettings.Origins)
        .AllowAnyHeader()
        .AllowAnyMethod()
    );
}

void RunDbMigrator(IApplicationBuilder app)
{
    using var startupScope = app.ApplicationServices.CreateScope();
    var migrationRunner = startupScope.ServiceProvider.GetService<IMigrationRunner>();
    migrationRunner?.MigrateUp();
}
