﻿namespace Locations.Web.Main.Output
{
    public class PresenterException : Exception
    {
        public PresenterException(string message) : base(message)
        {

        }
    }
}
