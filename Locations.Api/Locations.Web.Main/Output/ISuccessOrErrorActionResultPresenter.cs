﻿using Locations.Domain.Output;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Output
{
    public interface ISuccessOrErrorActionResultPresenter<in TSuccess, in TError> : ISuccessOrErrorPresenter<TSuccess, TError>
    {
        public IActionResult Render();
    }
}
