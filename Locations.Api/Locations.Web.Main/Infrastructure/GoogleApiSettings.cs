﻿using Locations.Domain;

namespace Locations.Web.Main.Infrastructure
{
    public class GoogleApiSettings: IGoogleApiSettings
    {
        public GoogleApiSettings(IConfiguration configuration)
        {
            configuration.ReadOnto("GoogleAuthentication", this);
        }
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}
