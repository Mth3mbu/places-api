﻿namespace Locations.Web.Main.Infrastructure
{
    public class AuthenticationSettings
    {
        public string TokenSigningKey { get; set; }
    }
}
