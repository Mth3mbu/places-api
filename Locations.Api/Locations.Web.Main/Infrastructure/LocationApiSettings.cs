﻿using Locations.Domain.jwt;

namespace Locations.Web.Main.Infrastructure
{
    public class LocationApiSettings : ILocationApiSettings
    {
        public LocationApiSettings(IConfiguration configuration)
        {
            configuration.ReadOnto("LocationsApiSettings", this);
        }
        public string SecretKey { get; set; }
    }
}
