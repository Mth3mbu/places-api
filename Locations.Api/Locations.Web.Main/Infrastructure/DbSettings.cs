﻿using Locations.Intergration.Sql.Connections;

namespace Locations.Web.Main.Infrastructure
{
    public class DbSettings : IDbSettings
    {
        private readonly IConfiguration _configuration;
        public DbSettings(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string ConnectionString => _configuration.GetConnectionString("PlacesDb");
    }
}
