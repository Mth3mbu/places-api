﻿using Locations.Domain.Location;
using Locations.Domain.Output;
using Locations.Domain.Search;
using Locations.Web.Main.Output;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Controllers
{
    [ApiController]
    [Route("search/{place}")]
    public class SearchController : ControllerBase
    {
        private readonly ISearchUseCase _searchUseCase;
        private readonly ISuccessOrErrorActionResultPresenter<List<Place>, ErrorDto> _presenter;
        public SearchController(ISearchUseCase searchUseCase, ISuccessOrErrorActionResultPresenter<List<Place>, ErrorDto> presenter)
        {
            _searchUseCase = searchUseCase;
            _presenter = presenter;
        }

        /// <summary>
        /// Get locations by name
        /// </summary>
        
        [HttpGet]
        public async Task<IActionResult> Get(string place)
        {
            await _searchUseCase.Excecute(_presenter, place);

            return _presenter.Render();
        }
    }
}
