﻿using Locations.Domain.Location;
using Locations.Domain.Location.UseCases;
using Locations.Domain.Output;
using Locations.Web.Main.Output;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Controllers
{
    [ApiController]
    [Route("places/{placeId}")]
    public class PlacesDetailsController : ControllerBase
    {
        private readonly IPlaceDetailsUseCase _placeDetailsUseCase;
        private readonly ISuccessOrErrorActionResultPresenter<PlaceDetails, ErrorDto> _presenter;
        public PlacesDetailsController(ISuccessOrErrorActionResultPresenter<PlaceDetails, ErrorDto> presenter, IPlaceDetailsUseCase placeDetailsUseCase)
        {
            _presenter = presenter;
            _placeDetailsUseCase = placeDetailsUseCase;
        }

        /// <summary>
        /// Get location details by placeId
        /// </summary>
        
        [HttpGet]
        public async Task<IActionResult> Get(string placeId)
        {
            await _placeDetailsUseCase.Excecute(_presenter, placeId);

            return _presenter.Render();
        }
    }
}


