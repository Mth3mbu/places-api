﻿using Locations.Domain.Location;
using Locations.Domain.Location.UseCases;
using Locations.Domain.Output;
using Locations.Web.Main.Output;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Controllers
{
    [ApiController]
    [Route("places/create")]
    [Authorize]
    public class CreatePlaceController : Controller
    {
        private readonly ISuccessOrErrorActionResultPresenter<IEnumerable<Place>, ErrorDto> _presenter;
        private readonly ICreatePlacesUseCase _createPlacesUseCase;
        public CreatePlaceController(ISuccessOrErrorActionResultPresenter<IEnumerable<Place>, ErrorDto> presenter, ICreatePlacesUseCase createPlacesUseCase)
        {
            _presenter = presenter; 
            _createPlacesUseCase = createPlacesUseCase; 
        }

        /// <summary>
        /// Creates a list of custom places. it requires authorization, it requires Jwt that is returned from Login endpoint
        /// </summary>

        [HttpPost]
        public async Task<IActionResult> Get(IEnumerable<Place> places)
        {
            await _createPlacesUseCase.Excecute(_presenter, places);

            return _presenter.Render();
        }
    }
}
