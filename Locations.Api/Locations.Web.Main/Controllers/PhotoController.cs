﻿using Locations.Domain.Output;
using Locations.Domain.Photos;
using Locations.Domain.Photos.UseCases;
using Locations.Web.Main.Output;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Controllers
{
    [ApiController]
    [Route("photos/{placeId}")]
    public class PhotoController : ControllerBase
    {
        private readonly IPhotoUseCase _photoUseCase;
        private readonly ISuccessOrErrorActionResultPresenter<List<Image>, ErrorDto> _presenter;
        public PhotoController(ISuccessOrErrorActionResultPresenter<List<Image>, ErrorDto> presenter, IPhotoUseCase searchUseCase)
        {
            _presenter = presenter;
            _photoUseCase = searchUseCase;
        }

        /// <summary>
        /// Get location photos by placeId, returns base64 string represantation of an image, use online tool base64 to image converter to view the actual image
        /// </summary>

        [HttpGet]
        public async Task<IActionResult> Get(string placeId)
        {
            await _photoUseCase.GetPhotosByPlaceId(_presenter, placeId);

            return _presenter.Render();
        }
    }
}
