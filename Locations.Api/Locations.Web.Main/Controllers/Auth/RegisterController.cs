﻿using Locations.Domain.Auth;
using Locations.Domain.Auth.UseCases;
using Locations.Domain.Output;
using Locations.Web.Main.Output;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Controllers.Auth
{
    [ApiController]
    [Route("auth/register")]
    public class RegisterController : ControllerBase
    {
        private readonly ISuccessOrErrorActionResultPresenter<User, ErrorDto> _presenter;
        private readonly IRegisterUseCase _registerUseCase;

        public RegisterController(ISuccessOrErrorActionResultPresenter<User, ErrorDto> presenter, IRegisterUseCase registerUseCase)
        {
            _presenter = presenter;
            _registerUseCase = registerUseCase;
        }

        /// <summary>
        /// Creates a user account 
        /// </summary>
        
        [HttpPost]
        public async Task<IActionResult> Register(User request)
        {
            await _registerUseCase.Excecute(_presenter, request);

            return _presenter.Render();
        }
    }
}
