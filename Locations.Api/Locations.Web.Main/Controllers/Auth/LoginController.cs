﻿using Locations.Domain.Auth;
using Locations.Domain.Auth.UseCases;
using Locations.Domain.Output;
using Locations.Web.Main.Output;
using Microsoft.AspNetCore.Mvc;

namespace Locations.Web.Main.Controllers.Auth
{
    [ApiController]
    [Route("auth/login")]
    public class LoginController : ControllerBase
    {
        private readonly ISuccessOrErrorActionResultPresenter<AuthResponse, ErrorDto> _presenter;
        private readonly ILoginUseCase _loginUseCase;

        public LoginController(ISuccessOrErrorActionResultPresenter<AuthResponse, ErrorDto> presenter, ILoginUseCase loginUseCase)
        {
            _presenter = presenter;
            _loginUseCase = loginUseCase;
        }

        /// <summary>
        /// Login, returns Auth Token
        /// </summary>
        
        [HttpPost]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            await _loginUseCase.Excecute(_presenter, request);

            return _presenter.Render();
        }
    }
}
