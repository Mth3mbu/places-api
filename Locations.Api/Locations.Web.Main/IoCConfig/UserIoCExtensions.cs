﻿using Locations.Domain.Users;
using Locations.Intergration.Sql.Users;

namespace Locations.Web.Main.IoCConfig
{
    public static class UserIoCExtensions
    {
        public static IServiceCollection AddUser(this IServiceCollection services)
        {
            services.AddScoped<IUserGateway, UserGateway>();

            return services;
        }
    }
}
