﻿using Locations.Web.Main.Output;

namespace Locations.Web.Main.IoCConfig
{
    public static class PresenterIoCConfig
    {
        public static IServiceCollection AddPresenters(this IServiceCollection services)
        {
            services.AddScoped(typeof(ISuccessOrErrorActionResultPresenter<,>), typeof(SuccessOrErrorRestPresenter<,>));

            return services;
        }
    }
}
