﻿using Locations.Domain.Location.UseCases;

namespace Locations.Web.Main.IoCConfig
{
    public static class PlacesUseCaseIoCExtensions
    {
        public static IServiceCollection AddPlaces(this IServiceCollection services)
        {
            services.AddScoped<IPlaceDetailsUseCase, PlaceDetailsUseCase>();
            services.AddScoped<ICreatePlacesUseCase, CreatePlacesUseCase>();

            return services;
        }
    }
}
