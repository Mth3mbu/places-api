﻿using Locations.Domain.Photos;
using Locations.Domain.Photos.UseCases;
using Locations.Intergration.Sql.Photo;

namespace Locations.Web.Main.IoCConfig
{
    public static class PhotoIoCExtensions
    {
        public static IServiceCollection AddPhotos(this IServiceCollection services)
        {
            services.AddScoped<IPhotoGateway, PhotoGateway>(); 
            services.AddScoped<IPhotoUseCase, PhotoUseCase>();

            return services;
        }
    }
}
