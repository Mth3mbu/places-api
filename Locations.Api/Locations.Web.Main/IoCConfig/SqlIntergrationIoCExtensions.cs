﻿using Locations.Domain.Location;
using Locations.Intergration.Sql.Connections;
using Locations.Intergration.Sql.Location;
using Locations.Web.Main.Infrastructure;

namespace Locations.Web.Main.IoCConfig
{
    public static class SqlIntergrationIoCExtensions
    {
        public static IServiceCollection AddSqlIntegration(this IServiceCollection services)
        {
            services.AddScoped<IDbSettings, DbSettings>();
            services.AddScoped<ILocationsDbConnectionContext>(
                provider => new AutoConnectingDbConnectionContext(provider.GetService<IDbSettings>(), settings => settings.ConnectionString));

            services.AddScoped<IPlacesGateway, PlacesGateway>();


            return services;
        }
    }
}
