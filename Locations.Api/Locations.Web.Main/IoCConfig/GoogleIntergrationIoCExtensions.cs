﻿using Locations.Domain;
using Locations.Domain.Search;
using Locations.Domain.Search.UseCases;
using Locations.Intergration.Google.Places;
using Locations.Web.Main.Infrastructure;

namespace Locations.Web.Main.IoCConfig
{
    public static class GoogleIntergrationIoCExtensions
    {
        public static IServiceCollection AddGooglePlaces(this IServiceCollection services)
        {
            services.AddScoped<IGooglePlacesGateway, GooglePlacesGateway>();
            services.AddScoped<ISearchUseCase, SearchUseCase>();
            services.AddScoped<IGoogleApiSettings, GoogleApiSettings>();

            return services;
        }
    }
}
