﻿using Locations.Domain.Auth;
using Locations.Domain.Auth.UseCases;
using Locations.Domain.jwt;
using Locations.Intergration.Sql.Auth;
using Locations.Web.Main.Infrastructure;

namespace Locations.Web.Main.IoCConfig
{
    public static class AuthIoCExtensions
    {
        public static IServiceCollection AddAuth(this IServiceCollection services)
        {
            services.AddScoped<ILoginUseCase, LoginUseCase>();
            services.AddScoped<IRegisterUseCase, RegisterUseCase>();
            services.AddScoped<ILocationApiSettings, LocationApiSettings>();
            services.AddScoped<IJwtTokenGenerator, JwtTokenGenerator>();
            services.AddScoped<IAuthGateway, AuthGateway>();

            return services;
        }
    }
}
