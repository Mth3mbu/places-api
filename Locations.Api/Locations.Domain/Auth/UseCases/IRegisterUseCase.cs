﻿using Locations.Domain.Output;

namespace Locations.Domain.Auth.UseCases
{
    public interface IRegisterUseCase
    {
        public Task Excecute(ISuccessOrErrorPresenter<User, ErrorDto> presenter, User user);
    }
}
