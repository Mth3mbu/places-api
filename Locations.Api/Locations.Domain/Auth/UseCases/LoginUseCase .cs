﻿using Locations.Domain.jwt;
using Locations.Domain.Output;

namespace Locations.Domain.Auth.UseCases
{
    public class LoginUseCase : ILoginUseCase
    {
        private readonly IAuthGateway _authGateway;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;

        public LoginUseCase(IAuthGateway authGateway,
            IJwtTokenGenerator jwtTokenGenerator)
        {
            _authGateway = authGateway;
            _jwtTokenGenerator = jwtTokenGenerator;
        }
        public async Task Excecute(ISuccessOrErrorPresenter<AuthResponse, ErrorDto> presenter, LoginRequest request)
        {
            var user = await _authGateway.Login(request);
            if (user == null)
            {
                presenter.Error(new ErrorDto { Message = "Invalid Login credentials" });
                return;
            }

            var verified = BCrypt.Net.BCrypt.Verify(request.Password, user.Password);
            if (!verified)
            {
                presenter.Error(new ErrorDto { Message = "Invalid Login credentials" });
                return;
            }

            var authToken = _jwtTokenGenerator.GenerateToken(user);
            presenter.Success(new AuthResponse { Token = authToken });
        }
    }
}
