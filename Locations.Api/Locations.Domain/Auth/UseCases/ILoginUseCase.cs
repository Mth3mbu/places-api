﻿using Locations.Domain.Output;

namespace Locations.Domain.Auth.UseCases
{
    public interface ILoginUseCase
    {
        public Task Excecute(ISuccessOrErrorPresenter<AuthResponse, ErrorDto> presenter, LoginRequest request);
    }
}
