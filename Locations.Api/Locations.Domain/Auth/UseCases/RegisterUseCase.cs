﻿using Locations.Domain.jwt;
using Locations.Domain.Output;
using Locations.Domain.Users;

namespace Locations.Domain.Auth.UseCases
{
    public class RegisterUseCase : IRegisterUseCase
    {
        private readonly IAuthGateway _authGateway;
        private readonly IUserGateway _userGateway;
       
        public RegisterUseCase(IAuthGateway authGateway,
            IJwtTokenGenerator jwtTokenGenerator,
            IUserGateway userGateway)
        {
            _authGateway = authGateway;
            _userGateway = userGateway;
        }
        public async Task Excecute(ISuccessOrErrorPresenter<User, ErrorDto> presenter, User user)
        {
            var existingUser = await _userGateway.FindByEmail(user.Email);
            if (existingUser != null)
            {
                presenter.Error(new ErrorDto { Message = $"{existingUser.Email} already exists" });
                return;
            }

            await CreateUser(user);

            presenter.Success(user);
        }

        private async Task<User> CreateUser(User user)
        {
            return await _authGateway.Register(new User
            {
                Id = Guid.NewGuid(),
                Email = user.Email,
                Name = user.Name,
                LastName = user.LastName,
                Password = BCrypt.Net.BCrypt.HashPassword(user.Password)
            });
        }
    }
}
