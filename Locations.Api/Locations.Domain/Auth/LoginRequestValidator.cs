﻿using FluentValidation;

namespace Locations.Domain.Auth
{
    public class LoginRequestValidator : AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(p => p.Email).EmailAddress()
                .WithMessage("Invalid email address")
                .NotNull().WithMessage("Please provide email address");

            RuleFor(p => p.Password).NotNull()
                .WithMessage("Please provide password")
                .MinimumLength(7)
                .WithMessage("Password must be atleast greater than 6 charecters");
        }
    }
}
