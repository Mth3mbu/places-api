﻿namespace Locations.Domain.Auth
{
    public class AuthResponse
    {
        public string Token { get; set; }
    }
}
