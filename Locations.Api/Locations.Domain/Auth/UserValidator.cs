﻿using FluentValidation;

namespace Locations.Domain.Auth
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(p => p.Email).NotEmpty()
                .WithMessage("Please provide an email address")
                .NotNull().EmailAddress()
                .WithMessage("Invalid email address");

            RuleFor(p => p.Id).NotEmpty()
                .NotNull()
                .WithMessage("User Id must not be null");

            RuleFor(p => p.LastName).NotEmpty()
                .WithMessage("Please provide last name")
                .NotNull()
                .WithMessage("Please provide last name");

            RuleFor(p => p.Name).NotEmpty()
                .WithMessage("Please provide name")
                .NotNull()
                .WithMessage("Please provide last name");

            RuleFor(p => p.Password).NotEmpty()
                .WithMessage("Please provide password")
                .MinimumLength(6)
                .WithMessage("Password must be greater than 6 charecters")
                .NotNull()
                .WithMessage("Please provide password");
        }
    }
}
