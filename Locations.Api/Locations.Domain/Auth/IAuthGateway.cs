﻿namespace Locations.Domain.Auth
{
    public interface IAuthGateway
    {
        Task<User> Register(User user);
        Task<User> Login(LoginRequest request);
    }
}
