﻿namespace Locations.Domain
{
    public interface IGoogleApiSettings
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}
