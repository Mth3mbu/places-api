﻿using Locations.Domain.Auth;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Locations.Domain.jwt
{
    public class JwtTokenGenerator : IJwtTokenGenerator
    {
        private readonly ILocationApiSettings _locationApiSettings;
        public JwtTokenGenerator(ILocationApiSettings locationApiSettings)
        {
            _locationApiSettings = locationApiSettings;
        }
        public string GenerateToken(User user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_locationApiSettings.SecretKey));
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
            var claims = new[] {
                new Claim("sub", user.Name ?? "BET"),
                new Claim("userId", user.Id.ToString()),
                 new Claim("email", user.Email)
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(30),
                SigningCredentials = signingCredentials
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
