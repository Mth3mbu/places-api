﻿namespace Locations.Domain.jwt
{
    public interface ILocationApiSettings
    {
        public string SecretKey { get; set; }
    }
}
