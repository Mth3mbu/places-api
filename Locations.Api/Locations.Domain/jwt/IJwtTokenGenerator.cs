﻿using Locations.Domain.Auth;

namespace Locations.Domain.jwt
{
    public interface IJwtTokenGenerator
    {
        string GenerateToken(User user);
    }
}
