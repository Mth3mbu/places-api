﻿namespace Locations.Domain.Photos
{
    public class Image
    {
        public Guid Id { get; set; }
        public string PlaceId { get; set; }
        public string Photo { get; set; }
    }
}
