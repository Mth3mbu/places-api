﻿using Locations.Domain.Output;

namespace Locations.Domain.Photos.UseCases
{
    public interface IPhotoUseCase
    {
        Task GetPhotosByPlaceId(ISuccessOrErrorPresenter<List<Image>, ErrorDto> presenter, string placeId);
    }
}
