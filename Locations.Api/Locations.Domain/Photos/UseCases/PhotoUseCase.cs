﻿using Locations.Domain.Location;
using Locations.Domain.Output;
using Locations.Domain.Search;

namespace Locations.Domain.Photos.UseCases
{
    public class PhotoUseCase : IPhotoUseCase
    {
        private readonly IGooglePlacesGateway _googlePlaceGateway;
        private readonly IPlacesGateway _placesGateway;
        private readonly IPhotoGateway _photoGateway;
        public PhotoUseCase(IGooglePlacesGateway googlePlaceGateway, IPhotoGateway photoGateway, IPlacesGateway placesGateway)
        {
            _googlePlaceGateway = googlePlaceGateway;
            _photoGateway = photoGateway;
            _placesGateway = placesGateway;
        }

        public async Task GetPhotosByPlaceId(ISuccessOrErrorPresenter<List<Image>, ErrorDto> presenter, string placeId)
        {
            var place = await _placesGateway.GetPlacesById(placeId);

            if (place == null)
            {
                presenter.Error(new ErrorDto
                {
                    Message = $"Invalid place Id:{placeId}"
                });

                return;
            }

            var photos = await _photoGateway.GetPhotosByPlaceId(placeId);

            if (photos.Count == 0)
            {
                var photoReferences = await _googlePlaceGateway.GetPhotoReferencesByPlaceId(placeId);
                var tasks = photoReferences.Select(photoRef => _googlePlaceGateway.GetPhotoByPhotoReferenceAndMaxWidth(photoRef));
                var response = await Task.WhenAll(tasks);

                photos = response.ToList()
                    .Select(photoUrl =>
                    new Image
                    {
                        Id = Guid.NewGuid(),
                        PlaceId = placeId,
                        Photo = photoUrl
                    }).ToList();

                await _photoGateway.CreatePhoto(photos);
            }

            presenter.Success(photos);
        }
    }
}
