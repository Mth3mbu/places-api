﻿namespace Locations.Domain.Photos
{
    public interface IPhotoGateway
    {
        Task<List<Image>> GetPhotosByPlaceId(string placeId);
        Task CreatePhoto(List<Image> photos);
    }
}
