﻿using Locations.Domain.Auth;

namespace Locations.Domain.Users
{
    public interface IUserGateway
    {
        Task<User> FindByEmail(string email);
        Task<User> FindByUserId(Guid userId);
    }
}
