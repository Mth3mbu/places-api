﻿using Locations.Domain.Location;

namespace Locations.Domain.Search
{
    public interface IGooglePlacesGateway
    {
        Task<List<Place>> GetPlacesByName(string name);
        Task<PlaceDetails> GetPlaceByPlaceId(string placeId);
        Task<string> GetPhotoByPhotoReferenceAndMaxWidth(string photoReference, int maxWidth = 400);
        Task<List<string>> GetPhotoReferencesByPlaceId(string placeId);
    }
}
