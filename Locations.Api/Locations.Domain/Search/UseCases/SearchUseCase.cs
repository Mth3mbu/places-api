﻿using Locations.Domain.Location;
using Locations.Domain.Output;

namespace Locations.Domain.Search.UseCases
{
    public class SearchUseCase : ISearchUseCase
    {
        private readonly IPlacesGateway _placeGateway;
        private readonly IGooglePlacesGateway _googlePlaceGateway;
        public SearchUseCase(IGooglePlacesGateway googlePlaceGateway, IPlacesGateway placeGateway)
        {
            _googlePlaceGateway = googlePlaceGateway;
            _placeGateway = placeGateway;
        }

        public async Task Excecute(ISuccessOrErrorPresenter<List<Place>, ErrorDto> presenter, string place)
        {
            var places = await _placeGateway.GetPlacesByName(place);

            if (places.Count > 0)
            {
                presenter.Success(places);
                return;
            }

            places = await _googlePlaceGateway.GetPlacesByName(place);

            if (places == null)
            {
                presenter.Error(new ErrorDto
                {
                    Message = $"{place} not found"
                });
                return;
            }

            presenter.Success(places);

            await _placeGateway.CreatePlaces(places);
        }
    }
}
