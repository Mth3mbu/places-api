﻿using Locations.Domain.Location;
using Locations.Domain.Output;

namespace Locations.Domain.Search
{
    public interface ISearchUseCase
    {
        Task Excecute(ISuccessOrErrorPresenter<List<Place>, ErrorDto> presenter, string place);
    }
}
