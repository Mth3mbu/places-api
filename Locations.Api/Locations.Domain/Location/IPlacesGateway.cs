﻿namespace Locations.Domain.Location
{
    public interface IPlacesGateway
    {
        Task CreatePlaces(IEnumerable<Place> places);
        Task<List<Place>> GetPlacesByName(string locationName);
        Task<Place> GetPlacesById(string placeId);
    }
}
