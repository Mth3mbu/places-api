﻿namespace Locations.Domain.Location
{
    public class PlaceDetails
    {
        public string FormatedAddress { get; set; }
        public string Icon { get; set; }
        public string GoogleMapsUrl { get; set; }
        public string Vicinity { get; set; }
        public int UctOffset { get; set; }
        public Address[] Addresses { get; set; }
        public string IconBackGroundColor { get; set; }
        public string IconBaseUri { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Reference { get; set; }
        public string[] Types { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string[] PhotoReferences { get; set; }
    }
}
