﻿namespace Locations.Domain.Location
{
    public class Photo
    {
        public string PlaceId { get; set; }
        public string Image { get; set; }
    }
}
