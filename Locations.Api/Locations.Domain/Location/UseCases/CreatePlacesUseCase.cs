﻿using Locations.Domain.Output;
using Locations.Domain.Users;

namespace Locations.Domain.Location.UseCases
{
    public class CreatePlacesUseCase : ICreatePlacesUseCase
    {
        private readonly IPlacesGateway _placesGateway;
        private readonly IUserGateway _userGateway;
        public CreatePlacesUseCase(IPlacesGateway placesGateway, IUserGateway userGateway)
        {
            _placesGateway = placesGateway;
            _userGateway = userGateway;
        }

        public async Task Excecute(ISuccessOrErrorPresenter<IEnumerable<Place>, ErrorDto> presenter, IEnumerable<Place> places)
        {
            var userId = places.ToArray()[0].UserId;
            var user = await _userGateway.FindByUserId(userId ?? Guid.Empty);

            if (user == null)
            {
                presenter.Error(new ErrorDto { Message = userId.HasValue ? $"Invalid userId {userId}" : "Please provider userId" });
                return;
            }

            await _placesGateway.CreatePlaces(places);

            presenter.Success(places);
        }
    }
}
