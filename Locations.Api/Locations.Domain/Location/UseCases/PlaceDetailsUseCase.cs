﻿using Locations.Domain.Output;
using Locations.Domain.Search;

namespace Locations.Domain.Location.UseCases
{
    public class PlaceDetailsUseCase : IPlaceDetailsUseCase
    {
        private readonly IGooglePlacesGateway _googlePlaceGateway;
        private readonly IPlacesGateway _placesGateway;
        public PlaceDetailsUseCase(IGooglePlacesGateway googlePlaceGateway, IPlacesGateway placesGateway)
        {
            _googlePlaceGateway = googlePlaceGateway;
            _placesGateway = placesGateway;
        }

        public async Task Excecute(ISuccessOrErrorPresenter<PlaceDetails, ErrorDto> presenter, string placeId)
        {
            var place = await _placesGateway.GetPlacesById(placeId);
            if (place == null)
            {
                presenter.Error(new ErrorDto { Message = $"Invalid PlaceId {placeId}, Please search the location first, then use the returned placeId" });
                return;
            }

            var details = await _googlePlaceGateway.GetPlaceByPlaceId(placeId);

            if (details == null)
            {
                presenter.Error(new ErrorDto { Message = $"No details found for location id {placeId}, Custom location details not supported yet!!" });
            }

            presenter.Success(details);
        }
    }
}
