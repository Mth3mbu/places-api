﻿using Locations.Domain.Output;

namespace Locations.Domain.Location.UseCases
{
    public interface IPlaceDetailsUseCase
    {
        Task Excecute(ISuccessOrErrorPresenter<PlaceDetails, ErrorDto> presenter, string placeId);
    }
}
