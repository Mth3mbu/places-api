﻿using Locations.Domain.Output;

namespace Locations.Domain.Location.UseCases
{
    public interface ICreatePlacesUseCase
    {
        Task Excecute(ISuccessOrErrorPresenter<IEnumerable<Place>, ErrorDto> presenter, IEnumerable<Place> places);
    }
}
