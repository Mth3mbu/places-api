﻿namespace Locations.Domain.Location
{
    public class Address
    {
        public string LongName { get; set; }
        public string ShortName { get; set; }
        public string[] Types { get; set; }
    }
}
