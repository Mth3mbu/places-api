﻿namespace Locations.Domain.Location
{
    public class Place
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Address { get; set; }
        public string SearchTerm { get; set; }
        public Guid? UserId { get; set; }
    }
}
