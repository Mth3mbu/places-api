﻿using Locations.Domain;
using Locations.Domain.Location;
using Locations.Domain.Search;
using Locations.Intergration.Google.Places.Models;
using RestSharp;
using Address = Locations.Domain.Location.Address;

namespace Locations.Intergration.Google.Places
{
    public class GooglePlacesGateway : IGooglePlacesGateway
    {
        private readonly IGoogleApiSettings _googleApiSettings;
        public GooglePlacesGateway(IGoogleApiSettings googleApiSettings)
        {
            _googleApiSettings = googleApiSettings;
        }

        public async Task<string> GetPhotoByPhotoReferenceAndMaxWidth(string photoReference, int maxWidth = 400)
        {
            var url = $"photo?maxwidth={maxWidth}&photo_reference={photoReference}";
            var response = await SendRequest<object>(url);

            return Convert.ToBase64String(response.RawBytes, Base64FormattingOptions.InsertLineBreaks);
        }

        public async Task<List<string>> GetPhotoReferencesByPlaceId(string placeId)
        {
            var url = $"details/json?place_id={placeId}&fields=photo";
            var response = await SendRequest<PhotosResponse>(url);

            return response.Data.result.photos.Select(photo => photo.photo_reference).ToList();
        }

        public async Task<PlaceDetails> GetPlaceByPlaceId(string placeId)
        {
            var query = $"details/json?place_id={placeId}";
            var results = await SendRequest<PlaceDetailsResponse>(query);
            var response = results?.Data?.result;

            if (response == null) return null;

            return new PlaceDetails
            {
                Addresses = response.address_components?.Select(x => new Address
                {
                    LongName = x.long_name,
                    ShortName = x.short_name,
                    Types = x.types
                }).ToArray(),
                PhotoReferences = response.photos?.Select(x => x.photo_reference).ToArray(),
                FormatedAddress = response.formatted_address,
                Name = response.name,
                Latitude = response.geometry.location.lat,
                Longitude = response.geometry.location.lng,
                Vicinity = response.vicinity,
                GoogleMapsUrl = response.url,
                Icon = response.icon,
                Types = response.types,
                UctOffset = response.utc_offset,
                Id = response.place_id,
                IconBackGroundColor = response.icon_background_color,
                IconBaseUri = response.icon_mask_base_uri,
                Reference = response.reference,
            };
        }

        public async Task<List<Place>> GetPlacesByName(string name)
        {
            const string nophotos = "No Photos";
            var query = $"textsearch/json?query={name}";
            var results = await SendRequest<PlaceResponse>(query);

            var places = results?.Data?.results.Select(x => new Place
            {
                Name = x.name,
                Address = x.formatted_address,
                Latitude = x.geometry.location.lat,
                Longitude = x.geometry.location.lng,
                Id = x.place_id,
                SearchTerm = name
            });

            return places.ToList();
        }

        private async Task<RestResponse<T>> SendRequest<T>(string query)
        {
            var url = $"{_googleApiSettings.BaseUrl}/{query}&key={_googleApiSettings.ApiKey}";
            var client = new RestClient(url);
            var request = new RestRequest();

            return await client.ExecuteAsync<T>(request);
        }
    }
}
