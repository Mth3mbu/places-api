﻿namespace Locations.Intergration.Google.Places.Models
{
    internal class Viewport
    {
        public Location northeast { get; set; }
        public Location southwest { get; set; }
    }
}
