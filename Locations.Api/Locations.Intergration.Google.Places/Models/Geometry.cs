﻿namespace Locations.Intergration.Google.Places.Models
{
    internal class Geometry
    {
        public Location location { get; set; }
        public Viewport viewPort { get; set; }
    }
}
