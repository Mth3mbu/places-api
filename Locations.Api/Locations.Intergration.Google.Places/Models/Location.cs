﻿namespace Locations.Intergration.Google.Places.Models
{
    internal class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
