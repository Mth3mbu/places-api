﻿namespace Locations.Intergration.Google.Places.Models
{
    internal class Details
    {
        public string formatted_address { get; set; }
        public string icon { get; set; }
        public string url { get; set; }
        public string vicinity { get; set; }
        public int utc_offset { get; set; }
        public Address[] address_components { get; set; }
        public string icon_background_color { get; set; }
        public string icon_mask_base_uri { get; set; }
        public string name { get; set; }
        public string place_id { get; set; }
        public string reference { get; set; }
        public Photo[] photos { get; set; }
        public string[] types { get; set; }
        public Geometry geometry { get; set; }

    }
}
