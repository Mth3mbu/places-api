﻿namespace Locations.Intergration.Google.Places.Models
{
    internal class PlaceResponse
    {
        public string[] html_attributions { get; set; }
        public string status { get; set; }
        public PlacesResponse[] results { get; set; }
    }
}
